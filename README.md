# Ubuntu 18.04/20.04 PHP FPM+SQLANYWHERE 16 (Sybase ASE)

## Summary

Container projetado para rodar o PHP 7.1 com um pool do PHP-FPM e suporte o driver para o SQLANYWHERE 16.

## Description

Para executar o php-fpm 7.1 com o nginx basta rodar o seguinte comando:

```bash
git clone https://gitlab.com/chimbida/ubuntu-php-sybase.git
docker-compose up
```

# Ubuntu 18.04 PHP 7.1 FPM+SQLANYWHERE 16 (Sybase ASE)

```
registry.gitlab.com/chimbida/ubuntu-php-sybase
registry.gitlab.com/chimbida/ubuntu-php-sybase:latest
```

# Ubuntu 20.04 PHP 5.6 FPM+SQLANYWHERE 16 (Sybase ASE)

```
registry.gitlab.com/chimbida/ubuntu-php-sybase:5.6
```