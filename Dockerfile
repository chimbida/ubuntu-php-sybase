FROM ubuntu:18.04

LABEL description="PHP 7.1-fpm e Sql Anywhere 16"
LABEL maintainer "https://gitlab.com.br/chimbida"

WORKDIR /tmp

RUN set -xe \
  && DEVBUILD="\
    git \
    build-essential \
    automake \
    bison \
    flex \
    libtool \
    re2c \
    apt-utils \
    language-pack-en-base \
    curl \
    unzip \
    software-properties-common" \
  && PACOTES="\
    php7.1-dev \
    php7.1-bcmath \
    php7.1-bz2 \
    php7.1-cli \
    php7.1-common \
    php7.1-curl \
    php7.1-cgi \
    php7.1-fpm \
    php7.1-gd \
    php7.1-gmp \
    php7.1-intl \
    php7.1-json \
    php7.1-ldap \
    php7.1-mbstring \
    php7.1-mcrypt \
    php7.1-odbc \
    php7.1-opcache \
    php7.1-redis \
    php7.1-phpdbg \
    php7.1-pspell \
    php7.1-readline \
    php7.1-recode \
    php7.1-soap \
    php7.1-tidy \
    php7.1-xml \
    php7.1-xmlrpc \
    php7.1-xsl \
    php7.1-zip \
    php-tideways" \
  && apt -y update \
  && DEBIAN_FRONTEND=noninteractive apt install -y $DEVBUILD --no-install-recommends --no-install-suggests \
  && locale-gen en_US.UTF-8 \
  && LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php \
  && DEBIAN_FRONTEND=noninteractive apt install -y $PACOTES --no-install-recommends --no-install-suggests \
  && echo "America/Sao_Paulo" | tee /etc/timezone \
  && ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime \
  && dpkg-reconfigure -f noninteractive tzdata \
  && mkdir -p /opt/sqlanywhere16 \
  && curl -L -O http://d5d4ifzqzkhwt.cloudfront.net/sqla16client/sqla16_client_linux_x86x64.tar.gz \
  && tar xzvf /tmp/sqla16_client_linux_x86x64.tar.gz -C /tmp \
  && /tmp/client1600/setup -silent -nogui -I_accept_the_license_agreement -sqlany-dir /opt/sqlanywhere16 \
  && curl -L -O http://d5d4ifzqzkhwt.cloudfront.net/drivers/php/sasql_php.zip \
  && unzip sasql_php.zip \
  && phpize \
  && ./configure --with-sqlanywhere \
  && make \
  && make install \
  && echo 'extension=sqlanywhere.so' >> /etc/php/7.1/mods-available/sqlanywhere.ini \
  && phpenmod sqlanywhere \
  && apt purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false $DEVBUILD php7.1-dev \
  && apt-get clean autoclean \
  && rm -rf /opt/sqlanywhere16/bin* \
  && rm -rf /opt/sqlanywhere16/support \
  && rm -rf /opt/sqlanywhere16/java \
  && rm -rf /opt/sqlanywhere16/sdk \
  && rm -rf /opt/sqlanywhere16/samples \
  && rm -rf /opt/sqlanywhere16/deployment \
  && rm -rf /opt/sqlanywhere16/thirdpartylegal \
  && rm -rf /opt/sqlanywhere16/*.txt \
  && rm -rf /opt/sqlanywhere16/lib64/php* \
  && rm -rf /opt/sqlanywhere16/lib32 \
  && rm -rf /var/lib/{apt,dpkg,cache,log}/ \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN sed -i "s/;date.timezone =.*/date.timezone = America\/Sao_Paulo/" /etc/php/7.1/fpm/php.ini \
  && sed -i "s/display_errors = Off/display_errors = On/" /etc/php/7.1/fpm/php.ini \
  && sed -i "s/upload_max_filesize = .*/upload_max_filesize = 10M/" /etc/php/7.1/fpm/php.ini \
  && sed -i "s/post_max_size = .*/post_max_size = 12M/" /etc/php/7.1/fpm/php.ini \
  && sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php/7.1/fpm/php.ini \
  && sed -i -e "s/pid =.*/pid = \/var\/run\/php7.1-fpm.pid/" /etc/php/7.1/fpm/php-fpm.conf \
  && sed -i -e "s/error_log =.*/error_log = \/proc\/self\/fd\/2/" /etc/php/7.1/fpm/php-fpm.conf \
  && sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.1/fpm/php-fpm.conf \
  && sed -i "s/listen = .*/listen = 9000/" /etc/php/7.1/fpm/pool.d/www.conf \
  && sed -i "s/;catch_workers_output = .*/catch_workers_output = yes/" /etc/php/7.1/fpm/pool.d/www.conf \
  && sed -i "s/pm.max_children = .*/pm.max_children = 10/" /etc/php/7.1/fpm/pool.d/www.conf \
  && sed -i "s/pm.start_servers = .*/pm.start_servers = 4/" /etc/php/7.1/fpm/pool.d/www.conf \
  && sed -i "s/pm.min_spare_servers = .*/pm.min_spare_servers = 2/" /etc/php/7.1/fpm/pool.d/www.conf \
  && sed -i "s/pm.max_spare_servers = .*/pm.max_spare_servers = 6/" /etc/php/7.1/fpm/pool.d/www.conf \
  && sed -i "s/;pm.status_path = .*/pm.status_path = \/php\/status/" /etc/php/7.1/fpm/pool.d/www.conf \
  && sed -i "s/;ping.path = .*/ping.path = \/php\/ping/" /etc/php/7.1/fpm/pool.d/www.conf \
  && sed -i "s/;ping.response = .*/ping.response = pong/" /etc/php/7.1/fpm/pool.d/www.conf \
  && echo "env[SQLANY16] = /opt/sqlanywhere16" >> /etc/php/7.1/fpm/pool.d/www.conf \
  && echo "env[LD_LIBRARY_PATH] = /opt/sqlanywhere16/lib64" >> /etc/php/7.1/fpm/pool.d/www.conf \
  && echo "env[PATH] = /usr/local/bin:/usr/bin:/bin:/opt/sqlanywhere16/bin64" >> /etc/php/7.1/fpm/pool.d/www.conf \
  && echo "/opt/sqlanywhere16/lib64" >> /etc/ld.so.conf.d/sqlanywhere16.conf \

ENV LD_LIBRARY_PATH=/opt/sqlanywhere16/lib64

EXPOSE 9000

CMD ["php-fpm7.1"]
